

Bulkpub module provides a set of XML-RPC web service APIs that support remote
bulk publication of external content to a Drupal site.
 
We have installed this module at www.ditainfo.info, where it is used to publish 
DITA-authored reference and tutorial documentation on a Drupal 7 site. 
This site illustrates an information mashup colocating structured and 
unstructured information symantically linked using common metadata.
 
The actual publication of the external files can be driven by client-side 
software that can publish, and later delete, all the files in one set as 
a unit.
 
The external files typically have these characteristics:
 1.There is a table of contents for all the individual files in the set
 2.The files are either text or xhtml format
 3.The files link to other files in the set
 4.The files may link to images
 5.The files contain metadata derived from keywords in the external source
 
Examples of such file formats would include:
 1.XHTML output produced by the DITA Open Toolkit
 2.XHTML output produced by processing DocBook source files to XHTML
 3.Eclipse Infocenter files
 4.HTML export files from a Framemaker book
 
The bulkpub module provides necessary APIs to publish such a file set as 
book pages in an outline with attached metadata tags and any required 
file-to-file links and embedded images.


Installation
------------

Copy bulkpub module to your sites/all/modules directory and then enable on the 
admin modules page.  

Author
------
Dick Johnson
rjohnson42@gmail.com
