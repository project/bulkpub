#
# PROLOG SECTION
# bulkdemo.py
#
# A sample script that demostrates how to use the Drupal 7
# bulkpub module.
#
# This simplified script stores a single page with some tags
# and a single image. Then the page and the image
# are deleted, along with the tags used.
#
# There is only minimal error checking done in this script,
# so use it only as a demonstration.
#
# In a real use case, many files and images would be
# uploaded and the results of the bulk operation
# stored in a results output file (I use XML) that
# could later be input to another script that
# would delete the entire batch.
#
# Tested with Python 3.2.2
# April 21, 2012
#
# Author: Dick Johnson
#         VR Communications, Inc.
#
###################################

###################################
# ENVIRONMENT SETUP SECTION
###################################

# import needed Python modules
import xmlrpc.client
import os
import io

###################################
# FUNCTION DEFINITION SECTION
###################################

#
# Function to list the supported XMLRPC methods
#
def listMethods():
    
    ret=proxy.system.listMethods()
    
    return ret

#
# Function to test for the existence of a method.
#
def testMethod(m):

    ret = proxy.system.listMethods()

    if m in ret:
        return True
    else:
        return False

#
# Function to get a list of the vocabularies
# that exist on the Drupal site.
#
def getVocabularies():
    
    vlist=[]
      
    try:
        vret = proxy.bulkpub.getVocabularies(user, password)
                
    except:
        print("bulkpub.getVocabularies failed")
        vret ="*error*"
        return vlist
    
        
    for v in vret:
        vlist.append(v)
        
    return vlist

#
# Function to get a list of content types
# this user can send to the server.
#
def getContentTypes():
    
    arg1 = "ignored"
    tplist=[]
            
    meth = proxy.bulkpub.getContentTypes
           
    try:
        ret = meth(arg1,user, password)
                
    except:
        print("bulkpub.getContentTypes failed")
        ret = []
            
    for v in ret:
        tplist.append(v['blogid'])
            
    return tplist

#
# Function to return the field information for a content type
#
def getFieldInfo(tp):
            
    try:
        vret = proxy.bulkpub.getFieldInfo(user, password,tp)
                
    except:
        print("bulkpub.getFieldInfo failed")
        vret = None
       
    return vret

#
# Function to store a new Drupal content type item
#
def newPage(pagedata):
    
    title = pagedata['title']
    page  = pagedata['content']
    terms = pagedata['keywords']
        
    # use the appropriate method
    meth = proxy.bulkpub.newPage
            
    # create the data to send over RPC
    #  title
    data = {"title":title}
    #  page content(text)
    data["description"] = page
    #  topic tags and chosen vocabulary
    if len(terms)>0:
        data['terms'] = terms
        data['vocabulary'] = vocabulary

    #  set alias
    if 'alias' in pagedata:
        data['alias'] = pagedata['alias']
    #  set text format
    if 'format' in pagedata:
        data['format'] = pagedata['format']
    
    # upload the page to Drupal
    try:
       ret=meth(content_type,user,password,data,True)
               
    except:
       ret=None
       print("newPage failed")
       
    return ret

#
# Function to delete a page
#
def deletePage(id):

    meth = proxy.bulkpub.deletePage

    try:
        ret=meth(content_type,id,user,password,True)
    except:
        print("deletePage failed")
        ret = None

    return ret

#
# Function to add a term to a vocabulary
# that exists on the site.
#
def addVocabularyTerm(vocab,term):
        
    try:
       ret = proxy.bulkpub.addVocabularyTerm(user, password,vocab,term)
              
    except:
       ret=None
       print("addVocabularyTerm error!",vocab,term)
        
    return ret

#
# Function to upload an image file as Drupal media.
# Returns the URL of the file on Drupal, or None if the
# upload fails.
#
def uploadImage(f):
    
    # labels for file types
    ftypes = {".png":"image/png", ".jpg":"image/jpeg", ".gif":"image/gif"}

    # initialize return
    ret=None

    # is the file there?
    if not os.path.exists(f):
        print("Error!",f,"does not exist")
        return ret

    # get the file type
    basef = os.path.basename(f)
    sp = os.path.splitext(basef)
    spext = sp[1]
    spext = spext.lower()

    # check for an image type we can handle
    if spext in ftypes:
        ftp = ftypes[spext]
    else:
        print("Error!",f,spext,"unknown image type")
        return None
            
    # start creating the data to send over XMLRPC    
    data = {'name': basef, 'type':ftp, 'bits': ""}

    # upload the image to Drupal
    try:
        fd = io.FileIO(f,mode='r+b')
    except:
        print("Error!",f,"could not be opened")
        return None

    # read in the file
    rawbits = fd.read()
    # convert it from binary to encoded text
    data['bits'] = xmlrpc.client.Binary(rawbits)

    sendf=False

    # use the appropriate method
    meth = proxy.bulkpub.newImage
            
    # upload the file to Drupal
    try:
      ret=meth(content_type,user,password,data)
      sendf=True
      
    except:
      print("newImage failed!")
      sendf =False
           
    # if the image was not sent, return an error
    if not sendf:
        return None
    # return the URL where the image was stored
    else:    
        return ret["url"]

#
# Function to delete an image file from Drupal.
#
def deleteImage(f):

    tailurl="xmlrpc.php"
    url = Drupalurl[:len(Drupalurl)-len(tailurl)]
    ff=f[len(url):]
    print(url,ff)

    try:
        ret=proxy.bulkpub.deleteImage(content_type,user,password,ff)
    except:
        print("deleteImage failed")
        ret=None
    
    return ret

###################################
# PROCESSING INITIALIZATION SECTION
###################################

# set the script global variables

# Drupal user credentials
user = "siteuser"
password = "siteuserpw"

# the XML-RPC url for the site
Drupalurl = "http://www.mydrupalsite.com/xmlrpc.php"

# the content type we will use
content_type = "article"

# the vocabulary for this content type
vocabulary = "tags"

# the name of a menu to test with
menuname = "menu-test-menu"

###################################
#
# MAIN PROCESSING SECTION
#
###################################

print("starting bulkdemo script")

# start server XML-RPC communication
print("starting communication with",Drupalurl)
proxy = xmlrpc.client.ServerProxy(Drupalurl, allow_none=True)

# list the methods we can call on the server
ret = listMethods()
print("Supported XMLRPC Methods")
for m in ret:
    print("  ",m)
print()

# Check for the Drupal bulkpub module present on the server
bulkpub_flag = testMethod('bulkpub.getVocabularies')

if bulkpub_flag:
    print("using the bulkpub module")
else:
    print("the bulkpub module is not available!")
    exit(0)
print()

# validate the vocabulary we are to use for metadata tags (terms)
try:
  vocab_list = getVocabularies()
except:
  print("getVocabularies failed")
  vocab_list = []
print("available vocabularies on the server")
for vv in vocab_list:
    print("  ",vv)
print()

if not vocabulary in vocab_list:
    print("Error, vocabulary",vocabulary,"is not in vocabularies")
else:
    print(vocabulary,"specified vocabulary exists on the server")
print()

# get the content types we can store on the server
ctypes = getContentTypes()
print(len(ctypes),"content types available to",user,"on the server")
for ct in ctypes:
    print("  ",ct)
print()

# validate the content type we want to use
if not content_type in ctypes:
    print("error",content_type,"is not in content types")
else:
    print(content_type,"content type exists on the server")
print()

# get the field information for the content type
fields = getFieldInfo(content_type)
print(content_type,"uses fields",fields)
print()

# create a new content type instance
testpage = {}
testpage['title'] = "Sample article"
testpage['content'] = "<h1>This is a sample article</h1>"
testpage['keywords'] = ["sample","article"]
testalias = "testpage"
testpage['alias'] = testalias
testpage['format'] = "filtered_html"

# add any terms to the vocabulary.
# they have to be added to the vocabulary before they
# are attached to a content instance.
for term in testpage['keywords']:
    print("adding term",term,"to",vocabulary)
    addVocabularyTerm(vocabulary,term)
print()

# send the new content instance
nodenum = newPage(testpage)
print("newPage created node",nodenum)
print()

# display the system path to the page alias
try:
  nodepath = proxy.bulkpub.getNodePath(testpage['alias'],user,password)
  print("page system path to",testalias,"is",nodepath)
except:
  nodepath = ""
  print("getNodePath failed")

# add a menu link
menuarg = {"name":menuname, "title":"testing", "node_path":nodepath}
menuarg['description'] = "menu description"

try:
  nmret = proxy.bulkpub.newMenuLink(content_type,user,password,menuarg)
  print("newMenuLink to menu",menuname,"returned",nmret)
except:
  print("newMenuLink failed")


# clear the test menu
try:
    mret = proxy.bulkpub.delAllMenuLinks(menuname, user, password)
    print("delAllMenuLinks for",menuname,"returned",mret)
except:
    print("delAllMenuLinks failed")

# delete the instance we just stored
ret = deletePage(nodenum)
print("deleteImage",nodenum,"returned",ret)
print()

# upload an image file
ifile = "carwash.jpg"
print("uploading",ifile)
ret = uploadImage(ifile)
print("uploadImage",ifile,"returns",ret)
print()

# delete the image
if not ret==None:
    print("delete image",ret)
    dret = deleteImage(ret)
    print("deleteImage returns",dret)
    print()

# delete orphan terms left in the vocabulary
try:
    terms = proxy.bulkpub.pruneVocabulary(user,password,vocabulary,1,10)
    print("vocabulary terms deleted",terms)
except:
    print("pruneVocabulary failed")

print("ending bulkdemo script")
